# Using npm

##Part 1

1. in cmder navigate to this directory,
2. inside run command `npm init` and answer questions,
3. check `package.json` file in VS Code

##Part 2

1. in cmder run `npm install bootstrap`,
2. verify that package was installed on `node_modules` directory,
3. using `npm install --save bootstrap`,
4. review `package.json` content

##Part 3

1. in cmder run `npm install --save-dev express`
2. examine `package.json`,
3. examine `node_modules` directory,
4. delete `node_modules` directory,
5. in cmder run `npm install --production`
6. examine `node_modules` directory.
  