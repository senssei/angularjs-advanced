/* global angular */
'use strict';

/**
 * @ngdoc function
 * @name 02ScaffoldingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the 02ScaffoldingApp
 */
angular.module('02ScaffoldingApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
