#Scaffolding

You need to have already installed first: 
 * node.js , npm , cmder (full version with git), 
 * additionally 
 
 	
	 npm install -g grunt-cli bower karma
 
##Part 0 
 
 Install all dependecies in cmder `npm install && bower install`
 
##Part 1 - Run app
 
1. run `grunt serve` in first cmder tab to run application
2. run `grunt test` in second cmder tab to run application
2. open app in Google Chrome `http://localhost:9000/#/`

## Part 2 - Examine project structure

1. examine folders structure, `app`, `test`
2. examine `package.json`, `bower.json`
3. examine `Gruntfile.js`, `test/karma.conf.js`

##Part 3 - Examine angular app base  

1. examine `app/index.html`,
2. examine `app/scripts/app.js`
3. examine `app/scripts/controllers/main.js`,
4. examine `test/spec/controllers/main.js`,

##Part 4 - Let's play
*For each change save file, then look at the consoles and browser.*

1. Change CSS in `app\styles\main.css` color palette - save,
2. Change JS in `app\scripts\controllers\main.js` by 
	* removing `;` and the end of line `16` - save and see serve tab in cmder
	* removing `this.awesomeThings` variable -  save and see test tab in cmder.  


