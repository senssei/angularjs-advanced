'use strict';

/**
 * @ngdoc directive
 * @name 02ScaffoldingApp.directive:reactiveList
 * @description
 * # reactiveList
 */
angular.module('02ScaffoldingApp')
  .directive('reactiveList', function () {
    return {
      template: '<div ng-transclude></div>',
      transclude: true,
      restrict: 'E'
    };
  });
