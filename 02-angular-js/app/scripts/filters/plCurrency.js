'use strict';

/**
 * @ngdoc filter
 * @name 02ScaffoldingApp.filter:plCurrency
 * @function
 * @description
 * # plCurrency
 * Filter in the 02ScaffoldingApp.
 */
angular.module('02ScaffoldingApp')
  .filter('plCurrency', function () {
    return function (input) {
      var fixed = input.toFixed(2); 
      return fixed + ' PLN';
    };
  });
