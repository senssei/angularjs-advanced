'use strict';

/**
 * @ngdoc service
 * @name 02ScaffoldingApp.magicString
 * @description
 * # magicString
 * Constant in the 02ScaffoldingApp.
 */
angular.module('02ScaffoldingApp')
  .constant('MAGIC_STRING', 42);
