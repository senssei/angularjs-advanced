#Angular in practice

#Scaffolding

You need to have already installed first: 
 * node.js , npm , cmder (full version with git), 
 * additionally 
 
  	
	 npm install -g grunt-cli bower karma
 
##Part 0 
 
 Install all dependecies in cmder `npm install && bower install`
 
1. run `grunt serve` in first cmder tab to run application
2. run `grunt test` in second cmder tab to run application
2. open app in Google Chrome `http://localhost:9000/#/`


## Step-01 Angular bootstrap

1. create directory `app/scripts`
2. inside create file `app.js`
3. add following bootstrap code
    ```
    'use strict';
	
	angular.module('02ScaffoldingApp',[]);
	```	
4. add this code in `index.html` line `86`
 	```
	<script src="scripts/app.js"></script>
	```
5. Last thing is to add `ng-app="02ScaffoldingApp"` attribute to the `body` element

*To test if it's working, just puts `{{2+2}}` in `index.html` ~ line `51` inside `body' element.`* 

## Step-02a Adding first controller

1. Create directory `app/controllers`
2. Create file `main.js`
3. Put this content in file
    ```
	'use strict';

	angular.module('02ScaffoldingApp')
	.controller('MainCtrl', function () {
		this.letters = ['a', 'b', 'c'];
	});
	```
4. In `index.html` below our last edit (line `51`) add
    ```
	<div ng-controller="MainCtrl as main">
      {{main.letters}}
    </div>
	```
5. **Don't forget** to attach our file in script references as in a `Step-01`

*This should render our scope variable in div element. Try modify your code. *

## Step-02b Write test to confirm functionality

1. Create directory `test/spec/controllers`
2. Inside create file `main.js`
3. Create test scaffold
   ```
   'use strict';

    describe('Controller: MainCtrl', function () {
		
	});
   ```
4. Now add module reference in `beforeEach` function inside `describe` function
   ```
   beforeEach(module('02ScaffoldingApp'));
   ``` 
5. After that inject controller `MainCtrl`
   ```
   var MainCtrl, scope;

  beforeEach(inject(function ($controller, $rootScope) {
     scope = $rootScope.$new();
     MainCtrl = $controller('MainCtrl', {
       $scope: scope
     });
   }));
   ```
6. Lastly add behaviour test in `it` function
	```
	it('should attach a list of letters to the scope', function () {
    	expect(MainCtrl.letters.length).toBe(3); 
	});
	```   

*Confirm that all is working in cmder.*

	PhantomJS 1.9.8 (Windows 8 0.0.0): Executed 1 of 1 SUCCESS (0 secs / 0.007 secs)
	PhantomJS 1.9.8 (Windows 8 0.0.0): Executed 1 of 1 SUCCESS (0.003 secs / 0.007 secs)

	Done, without errors.
	
	
## Step-03a Create sample injectable const serice

1. Create directory `app/services`
2. Create file `magicString.js`
3. Create constant service
	```
	'use strict';

	angular.module('02ScaffoldingApp')
	.constant('MAGIC_STRING', 42);
	```
4. **Don't forget** to attach our file in script references as in a `Step-01`

## Step-03b Add test to confirm functionality

1. Create directory `test/spec/services`
2. Inside create file `magicString.js`
3. Create test scaffold - see Step-0
4. Now add module reference in `beforeEach` function inside `describe` function - see `Step-02b`
5. After that just inject constant service `MAGIC_STRING` without `$scope` nor controller - similar to see `Step-02b` 
6. Add `it` function and using `expect` assertion test it against value `42`   
 
## Step-03c Inject cost to controller and use it on template

1. In `main.js` pass `MAGIC_STRING` as parameter to controller function,
2. Attach const value to scope variable
	```
	this.magic = MAGIC_STRING;
	```
3. In `index.html` add binding to `magic` scope variable.
4. Add additional integration test to `main.js` test to cover existence of `magic` on controller scope.

## Step-04 Injectable service

1. Let's start with creating file `powerService.js` for service in `app/services` directory
	```
	'use strict';

	angular.module('02ScaffoldingApp')
  	.service('powerCalc', function () {
    
		var api  = {
      		add: add
    	};
    	return api;

    	function add(a, b) {
    		return a + b;
    	}
	});

	```
2. Now based on `Step-03b` create test files `powerCalc.js` in `test/spec/services` to confirm
	* api is fully defined (`add` method exists) 
	* adds two numbers corectly
3. Using knowlegde from `Step-03c` inject this to `main.js` controller
4. and expose method to in html via
	```
	 {{main.func(3,2)}}	
	``` 


## Step-05a Using filters

1. In controller add new scope variable called `numbers` with values `1,2,3,4,5`  
2. In html bind `second` element of table `i=1`
3. Use `currency` filter on it
	```
	<div>
		{{main.numbers[1] | currency }}
	</div>
	```
4. It should look like this
```
$2.00
```

## Step-05b Using custom filters

1. Create new directory `app\scripts\filters`
2. add `plCurrency.js` file for filter 
	```
	'use strict';
	
	angular.module('02ScaffoldingApp')
	.filter('plCurrency', function () {
		return function (input) {
			var fixed = input.toFixed(2); 
			return fixed + ' PLN';
    	};
	});
	
	```
3. Use this custom filter instead if `currency`,
4. On controller  
	* inject this filter, 
	* create scope variable,
	* manually run filter on it,
	* bind it to html
5. Create appropiate test to cover functionality of this filter, based on previous tasks.


## Step-06a Using existing directives

1. Use `ng-repeat` directive to create list of previously created scope variable called `numbers`.
2. Use `ng-model` divective and `filter` filter to create simple search.
 	
## Step-06b Create custom directive

1. Create new directory `app\scripts\directives`
2. add `reactiveList.js` directive
	```
	'use strict';
	
	angular.module('02ScaffoldingApp')
	.directive('reactiveList', function () {
		return {
		template: '<div ng-transclude></div>',
		transclude: true,
		restrict: 'E'
		};
	});
	
	``` 
3. In `index.html` 
```
 <reactive-list>{{main.val}}</reactive-list>
```
4. At the end test compilation phase of this directive
```
 it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<reactive-list></reactive-list>');
    element = $compile(element)(scope);
    expect(element[0].innerHTML).toBe('<div ng-transclude=""></div>');
  }));
```





## If you want homework - ask me about... 

1. boostrap angular
2. controlles 
	* Scope, Controlers as
3. service
	* const, factory, service
4. scope
	* manual watch, deep watch,
5.  di
	* injectr errors, js sigleton,
	* config, run phase
6. filters, 
	* manual filter,
7.  directives
	* build-in
	* simple example,
	* restrict, scope, template, 
	* link,
	* transclude, 
	* composition
8.  Backend
	* manual promise - $q,
	* $http,
	* $resource
	
	
