'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('02ScaffoldingApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of letters to the scope', function () {
    expect(MainCtrl.letters.length).toBe(3); 
  });
  
  it('should attach a MAGIC_STRING to the scope', function () {
    expect(MainCtrl.magic).toBe(42); 
  });
  
   
  it('should attach val using custom filter', function () {
    expect(MainCtrl.val).toBe('5.00 PLN'); 
  });
});
