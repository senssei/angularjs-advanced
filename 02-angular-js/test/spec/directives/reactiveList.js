'use strict';

describe('Directive: reactiveList', function () {

  // load the directive's module
  beforeEach(module('02ScaffoldingApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<reactive-list></reactive-list>');
    element = $compile(element)(scope);
    expect(element[0].innerHTML).toBe('<div ng-transclude=""></div>');
  }));
});
