'use strict';

describe('Filter: plCurrency', function () {

  // load the filter's module
  beforeEach(module('02ScaffoldingApp'));

  // initialize a new instance of the filter before each test
  var plCurrency;
  beforeEach(inject(function ($filter) {
    plCurrency = $filter('plCurrency');
  }));

  it('should return the input sufixed with "plCurrency filter:"', function () {
    var text = 2;
    expect(plCurrency(text)).toBe('2.00 PLN');
  });

});
