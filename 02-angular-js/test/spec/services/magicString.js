'use strict';

describe('Service: magicString', function () {

  // load the service's module
  beforeEach(module('02ScaffoldingApp'));

  // instantiate service
  var MAGIC_STRING;
  beforeEach(inject(function (_MAGIC_STRING_) {
    MAGIC_STRING = _MAGIC_STRING_;
  }));
  
  it('should be 42', function () {
    expect(MAGIC_STRING).toBe(42);
  });

});
