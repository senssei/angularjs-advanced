'use strict';

describe('Service: powerCalc', function () {

  // load the service's module
  beforeEach(module('02ScaffoldingApp'));

  // instantiate service
  var powerCalc;
  beforeEach(inject(function (_powerCalc_) {
    powerCalc = _powerCalc_;
  }));

  it('should do something', function () {
    expect(!!powerCalc).toBe(true);
  });

  describe('add function', function () {
    it('should be defined', function () {
      expect(!!powerCalc.add).toBe(true);
    });
    
    it('should add two numbers', function () {
      expect(powerCalc.add(2,1)).toBe(3);
    });
  });

});
