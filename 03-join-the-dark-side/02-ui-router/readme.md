# ui-router in angular app
 
##Part 0

Install all dependecies in cmder. 
	
	npm install 
	
##Part 1 - Run app
 
1. Got to `sample` directory 
2. run `npm start` 
3. open app in Google Chrome `http://localhost:3000/`

##Part 2 - Review sample

1. Open `app.js` and see how `'$stateProvider'` and  `'$urlRouterProvider'` have been used,
2. Review templates construction in `index.html` and `contract` directory
