# JWT

http://jwt.io/
![comparing-jwt-vs-saml2.png](https://cdn.auth0.com/blog/legacy-app-auth/legacy-app-auth-5.png)

## Basic usage

![jwt-diagram.png](https://cdn.auth0.com/content/jwt/jwt-diagram.png)

## Architecture OAuth2
![oauth_jwt_flow.png](https://docs.oracle.com/cd/E39820_01/doc.11121/gateway_docs/content/images/oauth/oauth_jwt_flow.png)

