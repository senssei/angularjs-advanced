#Angular.js - advance course

This course consists of 3 separate modules:
1. JS developer - Cutting edge
2. Angular.js - In depth
3. Join the Dark Side - Applied Angular.js

## JS developer - Cutting edge

*Prerequisite is basic knowledge of web development - HTML, CSS and JavaScript.*

First module focuses on delivering solid, detailed and holistic information about modern JS: 
* tools, 
* stack, 
* frontend frameworks, 
* changes in language itself - ES 2015, 
* modularization recipes

##Angular.js - In depth

*Prerequisite is angular.js tutorial from https://docs.angularjs.org/tutorial*

Second module is centred around angular.js  from the basic concepts to most complicated 
mechanisms. Approaching framework pragmatic and touch topics like:
* core concepts, 
* data binding, 
* controllers,
* services, 
* scope,
* di container, 
* templates,
* filters,
* directives,
* backend integration  
 
 
##Join the Dark Side - Applied Angular.js

*Prerequisite is angular.js tutorial from https://docs.angularjs.org/tutorial*

Last module covers usage of angular.js in practise, to deliver large scope of web applications.
Main focus here is to give large number of practical examples:
* bussiness class applications,
* Single Page applications, 
* Hybrid Mobile Apps,
* backend integration, 
* security 

All of then can be reuse in different environments and projects.




